import './App.css'
import "leaflet/dist/leaflet.css";
import "react-leaflet-fullscreen/styles.css";
import Map from "./components/map/map.tsx";
import {QueryClient, QueryClientProvider} from 'react-query';

const queryClient = new QueryClient()

function App() {

    return (
        <QueryClientProvider client={queryClient}>
            <Map/>
        </QueryClientProvider>
    )
}

export default App
