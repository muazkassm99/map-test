import {MapPosition} from "./map-position.ts";


export type Marker = {
    id: number;
    position: MapPosition;
}
