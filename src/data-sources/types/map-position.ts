import {LatLngExpression} from "leaflet";

export type MapPosition = LatLngExpression;

