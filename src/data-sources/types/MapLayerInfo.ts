import L from "leaflet";

export type MapLayerInfo = {
    imageUrl: string;
    layer: L.TileLayer;
    name: string;
}
