import {Marker} from "../types/marker.ts";

function getExampleMarkers(): Promise<Marker[]> {
    return new Promise<Marker[]>((resolve) => {
        setTimeout(() => {
            const markers: Marker[] = [
                {
                    id: 1,
                    position: [51.505, -0.19],
                },
                {
                    id: 2,
                    position: [51.505, 0.19],
                },
                {
                    id: 3,
                    position: [51.505, -0.07],
                },
                {
                    id: 4,
                    position: [51.505, -0.08],
                }
            ];
            resolve(markers);
        }, 2000);
    });
}

const MarkerService = {
    getExampleMarkers,
}

export default MarkerService;
