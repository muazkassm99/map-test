import {create} from "zustand";
import createSelectors from "../utils/createSelectors.ts";
import {MapPosition} from "../data-sources/types/map-position.ts";
import DEFAULTS from "../utils/constants/DEFAULTS.ts";
import {MapLayerInfo} from "../data-sources/types/MapLayerInfo.ts";
import L from "leaflet";


type State = {
    mapCenter: MapPosition;
    isClustered: boolean;
    withPingIndication: boolean;
    isFullScreen: boolean;
    availableMapLayersInfo: MapLayerInfo[],
    selectedMapLayer: L.Layer,
}

type MapParametersStore = State;

export const useMapParametersStore = createSelectors(create<MapParametersStore>((() => ({
    mapCenter: DEFAULTS.MAP_CENTER,
    isClustered: true,
    isFullScreen: false,
    withPingIndication: false,
    availableMapLayersInfo: DEFAULTS.MAP_LAYER_INFOS,
    selectedMapLayer: DEFAULTS.SELECTED_LAYER
}))));

export const toggleFullScreen = () => useMapParametersStore.setState((state) => ({
    isFullScreen: !state.isFullScreen
}));

export const toggleIsClustered = () => useMapParametersStore.setState((state) => ({
    isClustered: !state.isClustered
}));
export const setIsClustered = (newValue: boolean) => useMapParametersStore.setState(() => ({
    isClustered: newValue
}));


export const toggleWithPingIndication = () => useMapParametersStore.setState((state) => ({
    withPingIndication: !state.withPingIndication
}));
export const setWithPingIndication = (newValue: boolean) => useMapParametersStore.setState(() => ({
    withPingIndication: newValue
}));

export const setMapCenter = (newCenter: MapPosition) => useMapParametersStore.setState(() => ({
    mapCenter: newCenter
}))

export const setSelectedMapLayer = (newLayer: L.Layer) => useMapParametersStore.setState(() => ({
    selectedMapLayer: newLayer
}))
