import {Marker} from "../data-sources/types/marker.ts";
import {create} from "zustand";
import createSelectors from "../utils/createSelectors.ts";


type State = {
    markers: Marker[];
}

type Action = {
    addMarker: (marker: Marker) => void;
    removeMarker: (id: number) => void;
    setMarkers: (markers: Marker[]) => void;
}

type MarkersStore = State & Action;

export const useMarkersStore = createSelectors(create<MarkersStore>((set) => ({
    markers: [],
    addMarker: (marker: Marker) => {
        set(state => ({
            markers: [...state.markers, marker]
        }))
    },
    removeMarker: (id: number) => {
        set((state) => ({
            markers: state.markers.filter(marker => marker.id !== id)
        }))
    },
    setMarkers: (markers: Marker[]) => {
        set(() => ({
            markers
        }))
    }
})));
