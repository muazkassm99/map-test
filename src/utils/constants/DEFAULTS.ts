import L, {LatLngExpression} from "leaflet";
import {MapLayerInfo} from "../../data-sources/types/MapLayerInfo.ts";

const MAP_CENTER: LatLngExpression = [51.505, -0.09];

const GOOGLE_LAYER = L.tileLayer('https://www.google.cn/maps/vt?lyrs=m@189&gl=cn&x={x}&y={y}&z={z}', {});
const OSM_LAYER = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {});

const LAYERS = {
    GOOGLE_LAYER,
    OSM_LAYER,
}

const MAP_LAYER_INFOS: MapLayerInfo[] = [
    {imageUrl: '/google.png', layer: LAYERS.GOOGLE_LAYER, name: 'Google Maps'},
    {imageUrl: '/osm.png', layer: LAYERS.OSM_LAYER, name: 'Open Street Maps'},
]

const DEFAULTS = {
    MAP_CENTER,
    MAP_LAYER_INFOS,
    LAYERS,
    SELECTED_LAYER: LAYERS.OSM_LAYER,
}
export default DEFAULTS;
