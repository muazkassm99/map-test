import L from 'leaflet';
import "leaflet.fullscreen"
import {useRef, useEffect} from "react";
import {useMap} from "react-leaflet";
import FullscreenEnter from '@mui/icons-material/Fullscreen'
import FullscreenExit from '@mui/icons-material/FullscreenExit'
import {toggleFullScreen, useMapParametersStore} from "../stores/map-parameters-store.ts";
import CustomIconButton from "../components/ui/CustomIconButton";

interface IFullScreenableMap extends L.Map {
    toggleFullscreen: () => void;
}

interface IFullScreenableHtmlElement extends HTMLElement {
    fullscreen: () => L.Control;
}

export function FullScreenControl() {

    const isFullScreen = useMapParametersStore.use.isFullScreen();
    const map = useMap() as IFullScreenableMap;

    const ctrl = useRef(((L.control as unknown) as IFullScreenableHtmlElement).fullscreen());

    useEffect(() => {
        ctrl.current.addTo(map);
        (ctrl.current as any).link.remove()

        return () => {
            (ctrl.current as any).link.remove();
            ctrl.current.remove();
        };
    });

    const handleFullscreenBtnClicked = () => {
        map.toggleFullscreen();
        toggleFullScreen();
    }


    return (
        <CustomIconButton
            tooltip={"Full Screen"}
            onClick={handleFullscreenBtnClicked}>
            {isFullScreen ? <FullscreenExit/> : <FullscreenEnter/>}
        </CustomIconButton>
    );
}
