import {useMap} from "react-leaflet";
import {ZoomIn, ZoomOut} from "@mui/icons-material";
import CustomButtonGroup from "../components/ui/CustomButtonGroup";
import CustomInnerButton from "../components/ui/CustomButtonGroup/CustomInnerButton.tsx";

export default function ZoomControl() {
    const m = useMap();

    const handleZoomIn = () => {
        m.zoomIn()
    }
    const handleZoomOut = () => {
        m.zoomOut()
    }
    return (
        <CustomButtonGroup>
            <CustomInnerButton onClick={handleZoomIn}>
                <ZoomIn/>
            </CustomInnerButton>
            <CustomInnerButton onClick={handleZoomOut}>
                <ZoomOut/>
            </CustomInnerButton>
        </CustomButtonGroup>
    )
}
