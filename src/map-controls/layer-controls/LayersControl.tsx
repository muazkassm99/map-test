import {useMap} from "react-leaflet";
import {useEffect} from "react";
import L from "leaflet";
import MapLayerSelectorPopover from "./partials/MapLayerSelectorPopover.tsx";
import {setSelectedMapLayer, useMapParametersStore} from "../../stores/map-parameters-store.ts";

export default function LayersControl() {
    const m = useMap();

    const selectedLayer = useMapParametersStore.use.selectedMapLayer();
    const availableMapLayersInfo = useMapParametersStore.use.availableMapLayersInfo();

    const layersArray = availableMapLayersInfo.map(o => o.layer);

    useEffect(() => {
        m.addLayer(selectedLayer);
    }, []);

    const handleMapChange = (layer: L.Layer) => {
        m.eachLayer(layer => {
            if (layersArray.includes(layer as never)) {
                m.removeLayer(layer)
            }
        })
        m.addLayer(layer);
        setSelectedMapLayer(layer)
    }


    return (
        <MapLayerSelectorPopover handleMapChange={handleMapChange}/>
    )
}
