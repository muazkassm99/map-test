import {Avatar, Button, ButtonGroup, IconButton, Popover, Typography} from "@mui/material";
import LayersIcon from "@mui/icons-material/Layers";
import React from "react";
import L from "leaflet";
import {useMapParametersStore} from "../../../stores/map-parameters-store.ts";
import CustomIconButton from "../../../components/ui/CustomIconButton";


interface MapLayerSelectorPopoverProps {
    handleMapChange: (layer: L.Layer) => void;
}

export default function MapLayerSelectorPopover({handleMapChange}: MapLayerSelectorPopoverProps) {

    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    const availableMapLayersInfo = useMapParametersStore.use.availableMapLayersInfo();
    const selectedMapLayer = useMapParametersStore.use.selectedMapLayer();


    return (
        <>
            <CustomIconButton
                tooltip={"Layers"}
                onClick={handleClick}
            >
                <LayersIcon/>
            </CustomIconButton>

            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
            >
                <ButtonGroup variant={"text"}>

                    {availableMapLayersInfo.map((mapInfo, index) => {
                        return (
                            <Button
                                key={index}
                                sx={{
                                    maxWidth: 100,
                                    display: 'flex',
                                    flexDirection: 'column',
                                    gap: '0.5rem',
                                    justifyContent: 'start'
                                }}
                                onClick={() => {
                                    handleMapChange(mapInfo.layer)
                                }}
                            >

                                <IconButton
                                    sx={{
                                        background: selectedMapLayer === mapInfo.layer ? "darkgray" : ""
                                    }}
                                >
                                    <Avatar src={mapInfo.imageUrl}/>
                                </IconButton>
                                <Typography>{mapInfo.name}</Typography>
                            </Button>
                        )
                    })}
                </ButtonGroup>
            </Popover>
        </>
    )
}
