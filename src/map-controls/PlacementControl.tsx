import React from "react";

interface ControlComponentProps {
    position: "top-left" | "top-right" | "bottom-left" | "bottom-right";
    children: React.ReactNode;
}

export default function PlacementControl({position, children}: ControlComponentProps) {

    let positionClass = "";
    if (position === "top-left") {
        positionClass = "leaflet-top leaflet-left";
    } else if (position === "top-right") {
        positionClass = "leaflet-top leaflet-right";
    } else if (position === "bottom-left") {
        positionClass = "leaflet-bottom leaflet-left";
    } else if (position === "bottom-right") {
        positionClass = "leaflet-bottom leaflet-right";
    }

    return (
        <div
            className={"leaflet-control-container"}
        >
            <div className={positionClass}>
                <div
                    className={"leaflet-control"}
                    style={{display: 'flex', gap: '0.5rem'}}
                >
                    {children}
                </div>
            </div>
        </div>
    )
}
