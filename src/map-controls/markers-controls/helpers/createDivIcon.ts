import {divIcon} from "leaflet";

export default function createDivIcon(withPing: boolean) {

    let toAdd = '';

    if (withPing) {
        toAdd = `
            <div class="divIcon-ping-wrapper">
                <svg style="transform:translate(7px, -10px)" width="10.5" height="10.5" viewBox="0 0 24 24"><path fill="#d32f2f" stroke="#000" stroke-width="2" d="M20 12l-18 12v-24z"></path></svg>
            </div>
`
    }
    const html = `
        <div class="divIcon-wrapper">
            <div class="divIcon-img-marker">
                <div style="width: 100%;height: 100%;background-image: url('/genericCarMarker.png');background-repeat: no-repeat; object-fit: contain; background-position: center; background-size: contain;"/>
            </div>
            ${toAdd}
        </div>
    `

    return divIcon({
        html,
        iconSize: [64, 64],
        iconAnchor: [64, 64],
        className: 'no-bg'
    });
}
