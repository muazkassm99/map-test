import {FormControlLabel, Popover, Stack, Switch} from "@mui/material";
import SettingsIcon from "@mui/icons-material/Settings";
import React from "react";
import {setIsClustered, setWithPingIndication, useMapParametersStore} from "../../../stores/map-parameters-store.ts";
import CustomIconButton from "../../../components/ui/CustomIconButton";


export default function TogglersPopover() {


    const isClustered = useMapParametersStore.use.isClustered();
    const withPingIndication = useMapParametersStore.use.withPingIndication();

    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;


    return (
        <>
            <CustomIconButton
                tooltip={"Settings"}
                onClick={handleClick}
            >
                <SettingsIcon/>
            </CustomIconButton>

            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
            >
                <Stack padding={5}>

                    <FormControlLabel
                        control={
                            <Switch
                                defaultChecked={isClustered}
                                onChange={(e) => {
                                    setIsClustered(e.target.checked)
                                }}
                            />
                        }
                        label="Toggle Cluster"
                    />

                    <FormControlLabel
                        control={
                            <Switch
                                defaultChecked={withPingIndication}
                                onChange={(e) => {
                                    setWithPingIndication(e.target.checked)
                                }}
                            />
                        }
                        label="Toggle Ping"
                    />
                </Stack>

            </Popover>
        </>
    )
}
