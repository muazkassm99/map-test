import {Button} from "@mui/material";
import {ButtonProps} from "@mui/material/Button/Button";

export default function CustomInnerButton(props: ButtonProps) {
    return (
        <Button
            variant="contained"
            sx={{
                borderRadius: 50,
                background: 'white',
                color: 'rgba(0, 0, 0, 0.54)',
                '&:hover': {
                    background: 'lightgray',
                    // boxShadow: 6,
                }
            }}
            {...props}
        >
            {props.children}
        </Button>
    )
}
