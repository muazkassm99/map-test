import {ButtonGroup} from "@mui/material"
import {ButtonGroupProps} from "@mui/material/ButtonGroup/ButtonGroup";

export default function CustomButtonGroup(props: ButtonGroupProps) {
    return (
        <ButtonGroup
            variant={"outlined"}
            aria-label="medium secondary button group"
            sx={{
                background: 'white',
                boxShadow: 3,
                borderRadius: '50px',
                '&:hover': {
                    // background: 'lightgray',
                    boxShadow: 6,
                }
            }}
            {...props}
        >
            {props.children}
        </ButtonGroup>
    )
}
