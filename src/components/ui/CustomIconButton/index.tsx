import {IconButton, Tooltip} from "@mui/material";
import {IconButtonProps} from "@mui/material/IconButton/IconButton";

type CustomIconButtonProps = IconButtonProps & {
    tooltip?: string
}
export default function CustomIconButton({tooltip, ...rest}: CustomIconButtonProps) {


    const ICON_COMPONENT = (
        <IconButton
            sx={{
                background: 'white',
                boxShadow: 3,
                '&:hover': {
                    background: 'lightgray',
                    boxShadow: 6,
                }
            }}
            {...rest}
        >
            {rest.children}
        </IconButton>
    );
    if (tooltip) {
        return (
            <Tooltip title={tooltip} placement="top-start">
                {ICON_COMPONENT}
            </Tooltip>
        )
    }
    return (
        ICON_COMPONENT
    )
}
