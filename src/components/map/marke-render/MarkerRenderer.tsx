import {useMapParametersStore} from "../../../stores/map-parameters-store.ts";
import ClusteredMarkersRenderer from "./partials/ClusteredMarkersRenderer.tsx";
import PlainMarkersRenderer from "./partials/PlainMarkersRenderer.tsx";

export default function MarkerRenderer() {

    const isClustered = useMapParametersStore.use.isClustered();


    if (isClustered) {
        return (
            <ClusteredMarkersRenderer/>
        )
    }
    return <PlainMarkersRenderer/>
}
