import {Marker} from "react-leaflet";
import {useMapParametersStore} from "../../../../stores/map-parameters-store.ts";
import {useMarkersStore} from "../../../../stores/markers-store.ts";
import createDivIcon from "../../../../map-controls/markers-controls/helpers/createDivIcon.ts";

export default function PlainMarkersRenderer() {
    const withPingIndication = useMapParametersStore.use.withPingIndication();
    const markers = useMarkersStore.use.markers();

    return (
        <>
            {markers.map((marker, index) => {

                const icon = createDivIcon(withPingIndication);
                return (
                    <Marker key={index} position={marker.position} icon={icon}></Marker>
                )
            })}
        </>
    )
}
