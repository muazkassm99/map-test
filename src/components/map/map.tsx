import {MapContainer} from "react-leaflet";
import PlacementControl from "../../map-controls/PlacementControl.tsx";
import ZoomControl from "../../map-controls/ZoomControl.tsx";
import LayersControl from "../../map-controls/layer-controls/LayersControl.tsx";
import MarkersControl from "../../map-controls/markers-controls/MarkersControl.tsx";
import {FullScreenControl} from "../../map-controls/FullScreenControl.tsx";
import {useMapParametersStore} from "../../stores/map-parameters-store.ts";
import MarkerService from "../../data-sources/fake-server/marker-service.ts";
import {useQuery} from "react-query";
import {useMarkersStore} from "../../stores/markers-store.ts";
import MarkerRenderer from "./marke-render/MarkerRenderer.tsx";

export default function Map() {

    const center = useMapParametersStore.use.mapCenter();
    const setMarkers = useMarkersStore.use.setMarkers();


    const {isLoading} = useQuery(
        ['map-markers-example'],
        () => MarkerService.getExampleMarkers(),
        {
            onSuccess: (markers) => {
                setMarkers(markers)
            }
        }
    );
    if (isLoading) {
        return (
            <>Loading</>
        )
    }

    return (
        <div style={{
            height: '600px',
        }}>
            <MapContainer

                center={center}
                zoom={13}
                scrollWheelZoom={true}
                style={{
                    height: '100%',
                    borderRadius: 20
                }}
                maxZoom={20}
                zoomControl={false}
            >
                <PlacementControl position={"bottom-left"}>
                    <ZoomControl/>
                    <LayersControl/>

                    <MarkersControl/>
                    <FullScreenControl/>
                </PlacementControl>

                <MarkerRenderer/>
            </MapContainer>
        </div>
    )
}
